<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
  
    protected $fillable = [
        'id',
        'name',
        'gender',
        'created_at'
    ];
    
}
