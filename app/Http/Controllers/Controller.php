<?php

namespace App\Http\Controllers;

use Aws\Api\Service;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Services\Services;
use App\People;
use Illuminate\Http\Request;

class Controller extends BaseController
{ 
    /**
     * Show actually date.
     * 
     * @return date
     */
    public function date()
    {
        $service = new Services();
        return $service->now();
    }
    /**
     * Store a person.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePeople(Request $request)
    {
        $service = new Services();
        return $service->store($request);
    }

    /**
     * Store a person.
     * @param  \Illuminate\Http\Request  $request and $id in BASE64
     * BASE64 is a id number store on database
     * @return \Illuminate\Http\Response
     */
    public function updatePeople(Request $request, $id)
    {

        $personId = base64_decode($id);
        $service = new Services();
        return $service->update($request, $personId);
    }
}
