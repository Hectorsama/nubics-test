<?php

namespace App\Http\Services;

use App\People;
use \Datetime;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class   Services
{

    /**
     * Show actually date.
     * @return \Illuminate\Http\Response
     */
    public function now()
    {
        try {
            $now = new DateTime;
            $dateString = $now->format('Y-m-d');
            $hourString = $now->format('H');
            $minuteString = $now->format('i');
            $date = array('Fecha' => $dateString, "Hora" => $hourString . ":" . $minuteString);
            $data = json_encode($date, JSON_UNESCAPED_UNICODE);
            return $data;
        } catch (ValidationException $e) {
            return response()->json(["Validation data error"], 302);
        }
    }

    /**
     * Store a person.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        try {
            $validate = $request->validate(['name' => 'required|string', 'age' => 'required|numeric', 'gender' => 'required|string']);
            $person = new People();
            $person->name = $request->name;
            $person->age = $request->age;
            $person->gender = $request->gender;
            $person->save();
        } catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(["error store"], 500);
        } catch (ValidationException $e) {
            return response()->json(["Validation data error"], 302);
        }
    }


    /**
     * Update a person.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $idPerson)
    {
        try {
            $validate = $request->validate(['age' => 'required|numeric', 'gender' => 'required|string']);
            $person = People::find($idPerson);
            $person->gender = $request->gender;
            $person->age = $request->age;
            $person->save();
        } catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(["error update"], 500);
        } catch (ValidationException $e) {
            return response()->json([$e], 302);
        }
    }
}
