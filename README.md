# Nubics Test
## Installation process
1. Install composer > Composer version 1.10.13
2. Install php >  7.3.22
## Execute
1. composer install
2. cp .env.example .env
3. php artisan key:generate
4. php artisan jwt:secret
5. php artisan migrate

## Start project

1. php artisan serve




This project is configured to be a REST API and uses Mysql as its database.
In the files, the database dump is available to be able to execute it.
It is necessary to put the crendeciales of the database in the .env.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
